package com.esv.event_evening_android.model.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.esv.event_evening_android.model.dao.CountryDao
import com.esv.event_evening_android.model.dao.EventDao
import com.esv.event_evening_android.model.entities.Country
import com.esv.event_evening_android.model.entities.Event
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Database(entities = [Event::class, Country::class], version = 2)
abstract class AppDatabase : RoomDatabase() {
    abstract fun productDao(): EventDao
    abstract fun countryDao(): CountryDao

    companion object {
        private lateinit var instance: AppDatabase
        private const val NUMBER_OF_THREADS = 4
        val databaseWriteExecutor: ExecutorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS)
        fun getInstance(context: Context): AppDatabase {
            instance = buildDatabase(context)
            return instance
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                "shopping_card"
            )
                .addCallback(object : RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                    }
                }).build()
        }
    }
}