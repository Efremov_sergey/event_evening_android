package com.esv.event_evening_android.model.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.*
import com.esv.event_evening_android.model.entities.Country

@Dao
interface CountryDao {
    @Query("SELECT * FROM country")
    fun selectAll(): PagingSource<Int, Country>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(country: List<Country>): List<Long>

    @Query("SELECT EXISTS(SELECT * FROM country)")
    fun isExists(): LiveData<Boolean>

    @Update
    fun updateCountry(country: Country)

    @Query("SELECT * FROM country WHERE event_id is null ORDER BY RANDOM() LIMIT 1")
    fun selectRandomCountry(): LiveData<Country>

    @Query("SELECT * FROM country WHERE numeric_code=:id")
    fun selectCountryById(id: Int): LiveData<Country>
}