package com.esv.event_evening_android.model.databinding

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("isGone", requireAll = false)
fun bindIsGone(view: View, isVisible: Boolean) {
    view.visibility = if (isVisible) {
        View.VISIBLE
    } else {
        View.GONE
    }
}

@BindingAdapter("dateText", requireAll = false)
fun bindDateText(view: TextView, date: Date?) {
    view.text = if (date == null)
        "Не выбрано"
    else
        SimpleDateFormat("dd.MM.yyyy", Locale.ROOT).format(date)
}