package com.esv.event_evening_android.model.reposiory

import android.app.Application
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.esv.event_evening_android.model.dao.EventDao
import com.esv.event_evening_android.model.database.AppDatabase
import com.esv.event_evening_android.model.entities.EventAndCountry

class EventListRepository(application: Application) {
    private val eventDao = AppDatabase.getInstance(application).productDao()

    fun loadFromDB() = Pager(
        PagingConfig(pageSize = 20)
    ) {
        eventDao.selectAll()
    }.flow

}