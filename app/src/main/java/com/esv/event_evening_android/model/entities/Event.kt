package com.esv.event_evening_android.model.entities

import androidx.room.*

@Entity
class Event(
    var date: Long,
    @ColumnInfo(name = "country_id")
    var countryId: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
}

data class EventAndCountry(
    @Embedded val event: Event,
    @Relation(
        parentColumn = "country_id",
        entityColumn = "numeric_code"
    )
    val country: Country
)