package com.esv.event_evening_android.model.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import retrofit2.http.Field


@Entity
data class Country(
    @PrimaryKey
    @ColumnInfo(name = "numeric_code")
    val numericCode: Int,
    val name: String,
    @ColumnInfo(name = "event_id")
    val eventId: Long? = null
)