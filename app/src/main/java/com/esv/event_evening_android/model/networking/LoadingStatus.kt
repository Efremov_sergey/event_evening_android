package com.esv.event_evening_android.model.networking

sealed class LoadingStatus {
    object Progress: LoadingStatus()

    data class Error(val message: String?) : LoadingStatus()

    data class Success<T>(val data: List<T>) : LoadingStatus()
}
