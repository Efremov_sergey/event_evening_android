package com.esv.event_evening_android.model.networking

import com.esv.event_evening_android.model.entities.Country
import retrofit2.http.GET

interface CountryApi {
    @GET("/rest/v2/all")
    suspend fun getCountryList(): List<Country>
}