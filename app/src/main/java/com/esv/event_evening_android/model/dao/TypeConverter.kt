package com.esv.event_evening_android.model.dao

import androidx.room.TypeConverter
import java.util.*

class TypeConverter {
    @TypeConverter
    fun dateToTimestamp(date: Date): Long {
        return date.time
    }
}