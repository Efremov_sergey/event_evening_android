package com.esv.event_evening_android.model.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.esv.event_evening_android.model.entities.Event
import com.esv.event_evening_android.model.entities.EventAndCountry

@Dao
interface EventDao {
    @Query("SELECT * FROM event")
    fun selectAll(): PagingSource<Int, EventAndCountry>

    @Insert
    fun insertAll(vararg events: Event?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(event: Event): Long
}