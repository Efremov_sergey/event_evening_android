package com.esv.event_evening_android.model.reposiory

import android.app.Application
import androidx.lifecycle.LiveData
import com.esv.event_evening_android.model.dao.EventDao
import com.esv.event_evening_android.model.database.AppDatabase
import com.esv.event_evening_android.model.entities.Country
import com.esv.event_evening_android.model.entities.Event
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AddEventRepository(application: Application) {
    private val eventDao = AppDatabase.getInstance(application).productDao()
    private val countryDao = AppDatabase.getInstance(application).countryDao()

    fun insertEvent(event: Event, country: Country) {
        AppDatabase.databaseWriteExecutor.execute {
            val id = this@AddEventRepository.eventDao.insert(event)
            this@AddEventRepository.countryDao.updateCountry(Country(country.numericCode, country.name, id))
        }
    }

    fun getCountry(): LiveData<Country> = countryDao.selectRandomCountry()

    fun getCountryById(id: Int): LiveData<Country> = countryDao.selectCountryById(id)
}