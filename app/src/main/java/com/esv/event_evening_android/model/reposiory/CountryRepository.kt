package com.esv.event_evening_android.model.reposiory

import android.app.Application
import com.esv.event_evening_android.model.dao.CountryDao
import com.esv.event_evening_android.model.database.AppDatabase
import com.esv.event_evening_android.model.entities.Country
import com.esv.event_evening_android.model.networking.CountryApiService
import com.esv.event_evening_android.model.networking.LoadingStatus
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException

class CountryRepository(application: Application) {
    private val apiService: CountryApiService = CountryApiService()
    private val countryDao: CountryDao = AppDatabase.getInstance(application).countryDao()

    val isCountryInCache = countryDao.isExists()

    val countryListFlow: Flow<LoadingStatus> = flow {
        emit(LoadingStatus.Progress)
        try {
            val list = apiService.getCountryList()
            AppDatabase.databaseWriteExecutor.execute {
                countryDao.insertAll(list)
            }
            emit(LoadingStatus.Success(list))
        } catch (e: Exception) {
            emit(LoadingStatus.Error(e.localizedMessage))
        } catch (e: HttpException) {
            emit(LoadingStatus.Error(e.localizedMessage))
        }
    }
}