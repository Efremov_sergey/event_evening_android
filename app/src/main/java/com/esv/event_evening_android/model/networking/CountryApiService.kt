package com.esv.event_evening_android.model.networking

import com.esv.event_evening_android.model.entities.Country
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CountryApiService {
    private val logging: HttpLoggingInterceptor = HttpLoggingInterceptor()
    private val client : OkHttpClient = OkHttpClient.Builder().apply {
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        this.addInterceptor(logging)
    }.build()


    private val api: Retrofit = Retrofit.Builder()
        .baseUrl(API_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    private val countryApi: CountryApi = api
        .create(CountryApi::class.java)


    suspend fun getCountryList(): List<Country> = countryApi.getCountryList()

    companion object {
        const val API_URL = "https://restcountries.eu"
    }
}