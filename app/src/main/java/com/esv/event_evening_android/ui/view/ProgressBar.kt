package com.esv.event_evening_android.ui.view

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.esv.event_evening_android.R
import java.util.*
import kotlin.collections.ArrayList

class ProgressBar: View {
    private val mDotRadius = 16.0f
    private val mBounceDotRadius = 16.0f
    private var mDotPosition = 0
    private val mDotAmount = 3
    private val listDots = listOf<View>(
        DotView(context),
        DotView(context),
        DotView(context)
    )
    private var viewHeight = 0
    private var viewWidth = 0
    private var animList: MutableList<ValueAnimator>? = null

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int, defStyleRes: Int) : super(context, attrs, defStyle, defStyleRes)
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    private var paint: Paint = Paint()
    //    private var dotResource: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ic_dot_progress)

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawIndicator(canvas, 3)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        generateAnim(getGraduateFloatList())
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        viewWidth = MeasureSpec.getSize(widthMeasureSpec)
        viewHeight = MeasureSpec.getSize(heightMeasureSpec)
        setMeasuredDimension(viewWidth, viewHeight)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    private fun getGraduateFloatList(): MutableList<Float?> {
        val floatList: MutableList<Float?> = mutableListOf()
        for (i in 1..3) {
            val a = i * 8
            floatList.add(a.toFloat())
        }
        floatList[floatList.size - 1] = floatList[0]
        return floatList
    }

    private fun generateAnim(floatList: MutableList<Float?>) {
        animList = ArrayList()
        for (i in 0 until 3) {
            Collections.shuffle(floatList)
            floatList[floatList.size - 1] = floatList[0]
            val floatArray = FloatArray(floatList.size)
            var j = 0
            for (f in floatList) {
                floatArray[j++] = f ?: Float.NaN
            }
            var anim = ValueAnimator()
            anim = ValueAnimator.ofFloat(*floatArray)
            anim.duration = 300
            anim.repeatCount = ValueAnimator.INFINITE
            anim.addUpdateListener { invalidate() }
            anim.start()
            animList?.add(anim)
        }
    }


    private fun drawIndicator(canvas: Canvas, barNum: Int) {
        val spaceWidth = canvas.width * 0.2 / (barNum - 1)
        val barWidth = canvas.width * 0.8 / barNum
        val sumWidth = spaceWidth + barWidth
        for (i in 0 until barNum - 1) {
            val height = animList!![i].animatedValue as Float
            canvas.drawCircle(
                (i * sumWidth).toFloat(),
                canvas.height.toFloat(),
                45f,
                paint
            )
//            canvas.drawRect(
//                (i * sumWidth).toFloat(),
//                canvas.height - height,
//                (i * sumWidth + barWidth).toFloat(),
//                canvas.height.toFloat(),
//                paint
//            )
            if (i == barNum - 2) {
                val heightLast = animList!![i + 1].animatedValue as Float
                canvas.drawRect(
                    ((i + 1) * sumWidth).toFloat(),
                    canvas.height - heightLast,
                    ((i + 1) * sumWidth + barWidth).toFloat(),
                    canvas.height.toFloat(),
                    paint
                )
            }
        }
    }
}

private class DotView: View {
    private val mDotRadius = 16.0f
    private val mBounceDotRadius = 16.0f

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int, defStyleRes: Int) : super(context, attrs, defStyle, defStyleRes)
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    private var paint: Paint = Paint()

    //    private var dotResource: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ic_dot_progress)

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        paint.color = resources.getColor(R.color.main_color, context.theme)

        createDot(canvas, paint)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        startAnimation()
    }

    private fun createDot(canvas: Canvas, paint: Paint) {
        canvas.drawCircle(mDotRadius * 2, 16f, mDotRadius, paint)
    }

    private fun startAnimation() {
        val progressAnimation = AnimationUtils.loadAnimation(context, R.anim.progress_animation)
        progressAnimation.repeatCount = Animation.INFINITE
        progressAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {}
            override fun onAnimationRepeat(animation: Animation) {}
        })
        startAnimation(progressAnimation)
    }
}