package com.esv.event_evening_android.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.esv.event_evening_android.R
import com.esv.event_evening_android.databinding.SplashFragmentBinding
import com.esv.event_evening_android.model.entities.Country
import com.esv.event_evening_android.model.networking.LoadingStatus
import com.google.android.material.snackbar.Snackbar

class SplashFragment : Fragment() {

    private lateinit var viewModel: SplashViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: SplashFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.splash_fragment, container, false)

        viewModel = ViewModelProvider(this).get(SplashViewModel(requireActivity().application)::class.java)

//        viewModel.isCountryInCache.observe(viewLifecycleOwner) {
//            if (it) {
//                navigateToEventViewPager(binding.root)
//            } else {
//                viewModel.countryList.observe(viewLifecycleOwner) { state ->
//                    when (state) {
//                        is LoadingStatus.Success<*> -> {
//                            tryCast<List<Country>>(state.data) {
//                                navigateToEventViewPager(binding.root)
//                            }
//                        }
//                        is LoadingStatus.Error -> Snackbar.make(requireView(), state.message ?: "Неизвестная ошибка", Snackbar.LENGTH_LONG).show()
//                        is LoadingStatus.Progress -> { /*TODO: show progress */ }
//                    }
//                }
//            }
//        }

        val progressAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.progress_animation)
        progressAnimation.repeatCount = Animation.INFINITE
        binding.imageViewLogo.startAnimation(progressAnimation)

        return binding.root
    }

    private fun navigateToEventViewPager(view: View) {
        Navigation.findNavController(view)
            .navigate(R.id.action_splashFragment_to_eventFragment)
    }

    // TODO: pass to extensions
    private inline fun <reified T> tryCast(instance: Any?, block: T.() -> Unit) {
        if (instance is T) {
            block(instance)
        }
    }

}