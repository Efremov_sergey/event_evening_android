package com.esv.event_evening_android.ui.event.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.esv.event_evening_android.R
import com.esv.event_evening_android.adapter.EventListAdapter
import com.esv.event_evening_android.databinding.FragmentEventListBinding
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest

class EventListFragment : Fragment() {
    private lateinit var mAdapter: EventListAdapter
    private lateinit var mViewModel: EventListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: FragmentEventListBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_event_list, container, false)

        mViewModel = EventListViewModel(requireActivity().application)
        EventListViewModel(requireActivity().application)
        mAdapter = EventListAdapter()

        binding.eventList.adapter = mAdapter
        binding.eventList.layoutManager = GridLayoutManager(requireContext(), 2)

        // TODO: добавить логику отображения статусов загрузки
//        mAdapter.addLoadStateListener {
//
//        }

        load()

        return binding.root
    }

    private fun load() {
        lifecycleScope.launchWhenStarted {
            mViewModel.loadData().collectLatest {
                mAdapter.submitData(it)
            }
        }
    }
}