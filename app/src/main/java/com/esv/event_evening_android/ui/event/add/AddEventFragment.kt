package com.esv.event_evening_android.ui.event.add

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.esv.event_evening_android.R
import com.esv.event_evening_android.databinding.AddEventFragmentBinding
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*

class AddEventFragment : Fragment() {

    private lateinit var mViewModel: AddEventViewModel
    //TODO: добавить реализацию возможности редактирования
//    private val args: AddEventFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: AddEventFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.add_event_fragment, container, false)

        mViewModel = AddEventViewModel(requireActivity().application)
        binding.viewModel = mViewModel
        binding.lifecycleOwner = this

//        args.eventAndCountry?.let {
//            mViewModel.date.postValue(Date(it.event.date))
//            mViewModel.getCountryById(it.country.numericCode)
//        }

        mViewModel.countryIsSelected.observe(viewLifecycleOwner) {
            if (it) binding.buttonGetCountry.text = getString(R.string.want_another)
        }

        binding.dateView.setOnClickListener {
            val calendar = Calendar.getInstance()
            val datePickerDialog = DatePickerDialog(requireContext(),
                { _, year, monthOfYear, dayOfMonth ->
                    val date = Calendar.getInstance()
                    date.set(year, monthOfYear, dayOfMonth)
                    mViewModel.date.postValue(date.time)
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)
            )
            datePickerDialog.show()
        }

        mViewModel.validationError.observe(viewLifecycleOwner) {
            Snackbar.make(requireView(), it, Snackbar.LENGTH_LONG).show()
        }

        mViewModel.onEventAdded.observe(viewLifecycleOwner) {
            navigateBack(requireView())
        }

        return binding.root
    }

    private fun navigateBack(view: View) {
        Navigation.findNavController(view).popBackStack()
    }
}