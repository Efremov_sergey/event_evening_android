package com.esv.event_evening_android.ui.event.add

import android.app.Application
import androidx.lifecycle.*
import androidx.lifecycle.Transformations.switchMap
import com.esv.event_evening_android.model.dao.TypeConverter
import com.esv.event_evening_android.model.entities.Country
import com.esv.event_evening_android.model.entities.Event
import com.esv.event_evening_android.model.reposiory.AddEventRepository
import java.util.*

class AddEventViewModel(application: Application) : AndroidViewModel(application) {

    private val mRepository: AddEventRepository = AddEventRepository(application)
    private val params = MediatorLiveData<Any>()

    val country: LiveData<Country> = switchMap(params) {
        // TODO: исправить путем передачи самой страны, а не брать из репозитория
        if (it is Int) {
            mRepository.getCountryById(it)
        } else {
            mRepository.getCountry()
        }
    }

    val countryIsSelected = Transformations.map(country) {
        it != null
    }

    val date = MutableLiveData<Date>()

    val validationError = MutableLiveData<String>()
    val onEventAdded = MutableLiveData<Boolean>()

    fun onButtonSelectCountryClick() {
        params.postValue(true)
    }

    fun onButtonCreateEventClicked() {
        if (validateFields()) {
            mRepository.insertEvent(Event(TypeConverter().dateToTimestamp(date.value!!), country.value!!.numericCode), country.value!!)
            onEventAdded.postValue(true)
        }
    }

    fun getCountryById(id: Int) {
        params.postValue(id)
    }

    private fun validateFields(): Boolean {
        if (country.value == null) {
            validationError.postValue(ERROR_NO_COUNTRY)
            return false
        }

        if (date.value == null) {
            validationError.postValue(ERROR_NO_DATE)
            return false
        }

        return true
    }

    companion object {
        const val ERROR_NO_COUNTRY = "Не выбрана страна"
        const val ERROR_NO_DATE = "Не выбрана дата"
    }
}