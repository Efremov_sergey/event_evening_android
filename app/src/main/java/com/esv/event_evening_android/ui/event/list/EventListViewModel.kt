package com.esv.event_evening_android.ui.event.list

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.esv.event_evening_android.model.entities.Event
import com.esv.event_evening_android.model.entities.EventAndCountry
import com.esv.event_evening_android.model.reposiory.EventListRepository
import kotlinx.coroutines.flow.Flow

class EventListViewModel(application: Application) : AndroidViewModel(application) {

    private val mRepository: EventListRepository = EventListRepository(application)

    fun loadData() = mRepository.loadFromDB()
}