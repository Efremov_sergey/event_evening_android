package com.esv.event_evening_android.ui.event.calendar

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.esv.event_evening_android.R

class EventCalendarFragment : Fragment() {

    companion object {
        fun newInstance() = EventCalendarFragment()
    }

    private lateinit var viewModel: EventCalendarViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.event_calendar_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(EventCalendarViewModel::class.java)
        // TODO: Use the ViewModel
    }

}