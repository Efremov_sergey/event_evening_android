package com.esv.event_evening_android.ui.splash

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import com.esv.event_evening_android.model.networking.LoadingStatus
import com.esv.event_evening_android.model.reposiory.CountryRepository
import kotlinx.coroutines.Dispatchers

class SplashViewModel(application: Application) : AndroidViewModel(application) {

    private val mRepository: CountryRepository = CountryRepository(application)

    val isCountryInCache = mRepository.isCountryInCache

    val countryList: LiveData<LoadingStatus> = liveData(Dispatchers.IO) {
        emitSource(mRepository.countryListFlow.asLiveData())
    }
}