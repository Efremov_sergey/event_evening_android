package com.esv.event_evening_android.ui.event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.esv.event_evening_android.R
import com.esv.event_evening_android.adapter.EventViewPagerStateAdapter
import com.esv.event_evening_android.databinding.EventFragmentBinding
import com.google.android.material.tabs.TabLayoutMediator

class EventFragment : Fragment() {

    private lateinit var viewModel: EventViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: EventFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.event_fragment, container, false)

        viewModel = ViewModelProvider(this).get(EventViewModel::class.java)

        binding.viewPagerEvent.adapter = EventViewPagerStateAdapter(requireActivity())
        binding.viewPagerEvent.isUserInputEnabled = true

        val mediator = TabLayoutMediator(binding.tabLayout, binding.viewPagerEvent) { currentTab, position ->
            currentTab.text = if (position == 0) getString(R.string.list) else getString(R.string.calendar)
            binding.viewPagerEvent.setCurrentItem(currentTab.position, true)
        }
        mediator.attach()

        binding.floatingActionButtonAddEvent.setOnClickListener {
            Navigation.findNavController(binding.root)
                .navigate(R.id.action_eventFragment_to_addEventFragment)
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                activity?.moveTaskToBack(true)
            }
        })

        return binding.root
    }
}