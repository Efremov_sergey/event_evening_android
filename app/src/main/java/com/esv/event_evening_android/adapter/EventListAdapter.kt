package com.esv.event_evening_android.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.esv.event_evening_android.R
import com.esv.event_evening_android.databinding.EventItemBinding
import com.esv.event_evening_android.model.entities.EventAndCountry
import com.esv.event_evening_android.ui.event.EventFragmentDirections
import java.text.SimpleDateFormat
import java.util.*

class EventListAdapter :
    PagingDataAdapter<EventAndCountry, EventListAdapter.ProductViewHolder>(
        EventListDiffCallback()
    ) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        // TODO: fix non optional
        val item: EventAndCountry = getItem(position)!!
        holder.bind(item)
    }

    class ProductViewHolder(
        parent: ViewGroup
    ) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.event_item, parent, false)
    ) {
        private var binding: EventItemBinding = EventItemBinding.bind(itemView)

        fun bind(eventAndCountry: EventAndCountry) {
            binding.textViewItemCountryName.text = eventAndCountry.country.name
            binding.textViewItemDate.text = SimpleDateFormat("dd.MM.yyyy", Locale.ROOT).format(eventAndCountry.event.date)

//            binding.root.setOnClickListener {
//                val bundle = bundleOf("event_and_country" to eventAndCountry)
//                Navigation.findNavController(binding.root)
//                    .navigate(R.id.action_splashFragment_to_eventFragment, bundle)
//            }
        }

    }

    private class EventListDiffCallback : DiffUtil.ItemCallback<EventAndCountry>() {

        override fun areItemsTheSame(
            oldItem: EventAndCountry,
            newItem: EventAndCountry
        ): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(
            oldItem: EventAndCountry,
            newItem: EventAndCountry
        ): Boolean {
            return oldItem.country.name == newItem.country.name && oldItem.event.date == newItem.event.date
        }
    }

}