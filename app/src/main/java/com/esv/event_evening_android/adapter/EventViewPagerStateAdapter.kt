package com.esv.event_evening_android.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.esv.event_evening_android.ui.event.calendar.EventCalendarFragment
import com.esv.event_evening_android.ui.event.list.EventListFragment

class EventViewPagerStateAdapter(
    activity: FragmentActivity,
): FragmentStateAdapter(activity) {

    override fun getItemCount(): Int = itemsCount

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                EventListFragment()
            }
            else -> {
                EventCalendarFragment()
            }
        }
    }

    companion object {
        const val itemsCount: Int = 2
    }
}