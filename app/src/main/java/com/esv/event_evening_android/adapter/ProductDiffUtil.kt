package com.esv.event_evening_android.adapter

import androidx.recyclerview.widget.DiffUtil
import com.esv.event_evening_android.model.entities.Event


class ProductDiffUtil : DiffUtil.ItemCallback<Event>() {
    override fun areItemsTheSame(
        oldItem: Event,
        newItem: Event
    ): Boolean {
        // Id is unique.
        return oldItem.date == newItem.date
    }

    override fun areContentsTheSame(
        oldItem: Event,
        newItem: Event
    ): Boolean {
        return oldItem.date == newItem.date
    }
}